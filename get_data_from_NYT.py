'''
The program get data from NYT API.
It parse articles and get the information into the database.
Information retrieved and match to existing DB fields:
    headline = title of the article
    pub_date = publish date time
    news_desk = news tab
    publisher = source
'''
import update_db
import requests
import datetime

FIRTS_ARTICLE = 0

NYT_API_URL = 'https://api.nytimes.com/svc/search/v2/articlesearch.json'
API_KEY_DB = '1bDr8Wn7CSUISsQ98reDrib7uT40nNLr'


def get_articles_parse(data):
    '''the function retreive the relevant fields from the data file and send it to the DB'''
    articles_data = dict()
    inserted_articles = 0

    for article in data['response']['docs']:
        if not inserted_articles:
            try:
                articles_data['title'] = article['headline']['main']
                articles_data['news_tab'] = article['news_desk']
                articles_data['publisher'] = article['source']
                articles_data['published_date'] = datetime.datetime.strptime(article['pub_date'], '%Y-%m-%dT%H:%M:%S+0000')
            except KeyError:
                print('error in get_articles_parse')
                continue

            # If the article doesn't exist in the DB, the function insert the data into the DB. \
            # It returns 1 if the article inserted and 0 otherwise
            inserted_articles += update_db.insert_articles_NYT(articles_data)
        else:
            break

    return inserted_articles


def get_NYT_data(word, date):
    """
    the function search for all articles from +/-30 days from published date that contains the first word
    from Ynet title. It retrieves the relevant data and insert to DB, including:title, pub_date, source and news_desk.
    """

    # e.g. for additional parameters https://pypi.org/project/NYTimesArticleAPInew/
    date = datetime.datetime.strptime(str(date), "%Y-%m-%d %H:%M:%S")
    q = word
    fq = {"headline": word}
    begin_date = date + datetime.timedelta(days=-30)
    end_date = max(date + datetime.timedelta(days=30), datetime.datetime.now())
    sort = 'newest'
    # fields that we would like to retrieve
    fl = ['headline', 'pub_date', 'source', 'news_desk']
    ## for QA: counts the results of our search according to the defined fields
    # facet_fields = ['section_name']
    # facet_filter = True

    # define the request parameters:
    query_params = {'q': q,
                    'fq': fq,
                    'begin_date': datetime.datetime.strftime(begin_date, '%Y%m%d'),
                    'end_date': datetime.datetime.strftime(end_date, '%Y%m%d'),
                    'sort': sort,
                    'fl': fl,
                    'page': 0,
                    # 'facet_fields': facet_fields,
                    # 'facet': facet_filter,
                    'api-key': API_KEY_DB
                    }

    request = requests.get(NYT_API_URL, params=query_params, timeout=6)
    data = request.json()

    inserted_articles = 0

    try:
        if data['response']['meta']['hits'] > 0:
            inserted_articles += get_articles_parse(data)
    except KeyError:
        print('error in data[response]')
        return inserted_articles

    return inserted_articles
