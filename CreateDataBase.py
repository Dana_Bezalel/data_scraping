"""
The program creates a Data Base "Ynet_Articles" and Tables "articles" for articles to be places on.
Data base structure:
id - primary key
news_tab - name of the news tab
title - string with the title of the article
publisher - name of article publisher
published_date - date that the article was first published
"""
import mysql.connector

# Define parameters
DANA_MYSQL_PASS = 'tuah@1215'
MOR_MYSQL_PASS = '029999307'
DB_NAME = "Articles"
YNET_ARTICLES_TABLE = "ynet_articles"
NYT_ARTICLES_TABLE = "nyt_articles"

con = mysql.connector.connect(user='root',
                              password=DANA_MYSQL_PASS,
                              host="localhost")

db_cur = con.cursor()

# Creates Database 'Ynet'
db_cur.execute("CREATE DATABASE IF NOT EXISTS {}".format(DB_NAME))
con.commit()

db_cur.execute("SHOW DATABASES")
print("Data Bases:\n")
for x in db_cur:
    print(x)

db_cur.execute("""USE {}""".format(DB_NAME))
con.commit()

# Creates table articles in 'Ynet' database
db_cur.execute("""CREATE TABLE IF NOT EXISTS {} (
               id INT AUTO_INCREMENT PRIMARY KEY,
               news_tab VARCHAR(255),
               title VARCHAR(255),
               publisher VARCHAR(255),
               published_date DATETIME)
               """.format(YNET_ARTICLES_TABLE))

# Creates table articles in 'NYT' database
db_cur.execute("""CREATE TABLE IF NOT EXISTS {} (
               id INT AUTO_INCREMENT PRIMARY KEY,
               ynet_id INT,
               news_tab VARCHAR(255),
               title VARCHAR(255),
               publisher VARCHAR(255),
               published_date DATETIME,
               FOREIGN KEY (ynet_id) REFERENCES {}(id))
               """.format(NYT_ARTICLES_TABLE, YNET_ARTICLES_TABLE))

con.commit()

db_cur.execute("SHOW TABLES")

print("\nTables in {}:".format(DB_NAME))
for table in db_cur:
    print(table)

print("Ynet table columns:\n")
db_cur.execute("SHOW COLUMNS FROM {}".format(YNET_ARTICLES_TABLE))
print("\nTable columns in '{}' table:".format(YNET_ARTICLES_TABLE, DB_NAME))
for column in db_cur.fetchall():
    print(column)

print("NYT table columns:\n")
db_cur.execute("SHOW COLUMNS FROM {}".format(NYT_ARTICLES_TABLE))
print("\nTable columns in '{}' table:".format(NYT_ARTICLES_TABLE, DB_NAME))
for column in db_cur.fetchall():
    print(column)

db_cur.close()
