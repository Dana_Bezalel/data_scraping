import mysql.connector
import datetime
import get_data_from_links
import get_data_from_NYT
from bs4 import BeautifulSoup

TIME1 = 2
DATE1 = 4
MOR_MYSQL_PASS = '029999307'
DANA_MYSQL_PASS = 'tuah@1215'
DB_NAME = "Articles"
FIRST_WORD = 0


def print_status(total_articles_dow, total_articles_up, enrich, nyt_articles):
    """the function receives an amount of articles and print status of total urls downloaded and total urls inserted
        at current time"""
    download_time_date = datetime.datetime.strftime(datetime.datetime.now(), "%d/%m/%Y, %H:%M")
    print(
        "YNET: Download date and time {}, Total links downloaded:{}, Total new links inserted to DB:{}".format(
            download_time_date,
            total_articles_dow,
            total_articles_up))
    if enrich == 'y':
        print("NYT: Enriched data from NYT: {}".format(nyt_articles))


def parse_content(url):
    """ the function gets a url = article and retrieve the data from url:
        title, published date, author"""
    # print(f'url:{url}')
    source_url = get_data_from_links.get_source_data(url)
    main_page_content = BeautifulSoup(source_url.content, 'html.parser')

    # there are two approaches to retrieve data due to recent change in the web site:
    try:
        if main_page_content.find(class_="mainTitle"):
            # approach 1:
            title = main_page_content.find(class_="mainTitle").get_text()
            author = main_page_content.find(class_="authors").get_text().strip('[| ]').replace(',', ' -')
            time_str = main_page_content.find(class_="originalLaunchDate").get_text().split()
            time_str = " ".join([time_str[DATE1], time_str[TIME1]])
            publish_date = datetime.datetime.strptime(time_str, "%m.%d.%y %H:%M")


        else:
            # approach 2:
            title = str(main_page_content.find_all(class_="art_header_title")).split('>')[1].split('<')[0]
            author = str(main_page_content.find_all(class_="art_header_footer_author")).split('>')[2].split('<')[0]
            date_str = \
                str(main_page_content.find_all(class_="art_footer_date")).split("\"art_footer_date\">")[1].split(": ")[
                    1].split('<')[0]
            date_str = datetime.datetime.strptime(date_str.strip().replace(",", ""), "%m.%d.%y %H:%M")
            publish_date = str(date_str)

        data = {'title': title, 'author': author, 'publish_date': publish_date}
        return data

    except:
        print("Oops! couldn't extract data from one of the links: {}".format(url))


def write_articles_data_to_db(news_page_urls, limit, enrich):
    """the function receives dictionary of new tab, gets the articles per news_tab and parse each article.
        The data is written into the DB with the following fields:
        News_Tab, Title, Publisher, Published_Date"""

    # insert date per new_tab
    con = mysql.connector.connect(user='root',
                                  password=DANA_MYSQL_PASS,
                                  host="localhost", database=DB_NAME)
    cur = con.cursor()
    count_uploaded = 0
    count_downloaded = 0
    count_nyt_uploaded = 0
    LIMIT_EXCEED = False

    for tab_name, links in news_page_urls.items():
        if limit and LIMIT_EXCEED:
            break
        articles_links = get_data_from_links.get_article_links(links, tab_name, 'slotTitle')
        for url in articles_links:
            if limit and limit <= count_uploaded:
                LIMIT_EXCEED = True
                break
            # parse articles data
            data = parse_content(url)
            if data:
                count_downloaded += 1
                cur.execute("SELECT id FROM ynet_articles WHERE news_tab = %s AND title = %s AND publisher = %s",
                            (tab_name, data['title'], data['author']))
                result = cur.fetchall()
                # if article isn't already inside
                if len(result) == 0:
                    count_uploaded += 1

                    cur.execute("INSERT INTO ynet_articles (news_tab, title, publisher, published_date) \
                                  VALUES (%s, %s, %s, %s)",
                                [tab_name, data['title'], data['author'], data['publish_date']])

                    con.commit()

                    if enrich == 'y':
                        # If enrich data selected, insert 1 article from NYT-API that was published within the range of +/- 30 days from published \
                        # data and contains the first word of the title into NYT table;
                        NYT_articles = get_data_from_NYT.get_NYT_data(data['title'].split()[FIRST_WORD],
                                                                      data['publish_date'])
                        count_nyt_uploaded += NYT_articles

    cur.close()
    # print status of total articles added
    print_status(count_downloaded, count_uploaded, enrich, count_nyt_uploaded)


def insert_articles_NYT(articles_data):
    """the function receives NYT articles and write it into the DB
        Input: dictionary with fields: news_tab, title, publisher, published_date
    """
    insert_data = False
    # insert date per new_tab
    con = mysql.connector.connect(user='root',
                                  password=DANA_MYSQL_PASS,
                                  host="localhost", database=DB_NAME)
    cur = con.cursor()

    cur.execute("SELECT id FROM nyt_articles WHERE news_tab = %s AND title = %s AND publisher = %s",
                (articles_data['news_tab'], articles_data['title'], articles_data['publisher']))

    result = cur.fetchall()
    # if article isn't already inside
    if len(result) == 0:
        insert_data = True

        cur.execute('SELECT MAX(id) FROM ynet_articles')
        ynet_id = cur.fetchall()[0][0]

        cur.execute("INSERT INTO nyt_articles (ynet_id, news_tab, title, publisher, published_date) \
                      VALUES (%s, %s, %s, %s, %s)",
                    (ynet_id, articles_data['news_tab'], articles_data['title'], articles_data['publisher'],
                     articles_data['published_date']))

        con.commit()

    cur.close()

    return 1 if insert_data else 0
