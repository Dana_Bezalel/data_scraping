from bs4 import BeautifulSoup
import re
import requests

LAST_LINKS = -2


def get_source_data(url):
    """the function gets page link;
        if successful responses (200) it returns the data content, otherwise prints an error"""
    link = requests.get(url, timeout=5)
    if link.status_code == requests.codes.ok:
        return link
    else:
        print("error with url: {}".format(url))


def get_news_tab_links(url_page, search_class, news_tab_to_retrieve):
    """the function gets Ynet main page url and a class_ type and returns a dictionary with the news tab as key and the url as a value
        Input variables: url_page - source url, search_class - class for retrieving data;
        news_tab_to_retrieve - if the user requested for a specific News Tab, the function will return it only
        Returns: dictionary of News_tab(s) and urls links;
    """

    # find all urls according to search class and type
    full_links = get_page_links(url_page, search_class)

    # returns list of links of articles under the main page
    news_tab_dict = dict()
    links = re.findall(r'href=\"(\S+)\"', full_links)
    names = re.findall(r'\>(\w+\s*\w+)\<', re.sub('&amp; ', "", full_links))
    # the 2 last News_Tab are not relevant, hence removed: Art, Espaniol
    links = links[:LAST_LINKS]
    names = names[:LAST_LINKS]
    for key, val in zip(names, links):
        news_tab_dict[key] = val
    if news_tab_to_retrieve:
        return {key: val for key, val in news_tab_dict.items() if key == news_tab_to_retrieve}
    else:
        return news_tab_dict


def get_page_links(url_page, search_class):
    """the function gets url page and a class_ type and returns a string of all match class
        Input variables: url_page - source url, search_class - class for retrieving data
        Returns: str of url class search"""

    source_url = get_source_data(url_page)
    main_page_content = BeautifulSoup(source_url.content, 'html.parser')

    # find all urls according to search class and type
    full_links = str(main_page_content.find_all(class_=search_class))

    return full_links


def get_article_links(news_page_url, news_tab, search_class):
    """the function gets News Tabs url and retrieves all articles links
        input: url_page - source url, search_class - class for retrieving data
       returns: list of all article urls"""

    full_links = get_page_links(news_page_url, search_class)
    article_list = re.findall(r'href=\"(\S+)\"', full_links)

    print("There are {} links in News Tab {}".format(len(article_list), news_tab))

    return article_list
