import argparse
def main():
    news_tab_opts = {'isn': 'Israel News', 'me': 'Middle East', 'w': 'World', 'm': 'Magazine', 'j': 'Jewish',
                     'o': 'Opinion',
                     'c': 'Culture', 't': 'Travel', 'h': 'Health', 's': 'Science', 'b': 'Business', 't': 'Tech'}
    news_tab_help = 'chose news_tab to add to the database from :' + str(news_tab_opts)
    parser = argparse.ArgumentParser()
    parser.add_argument('--news_tab', choices=news_tab_opts.keys(), type=str, help=news_tab_help)
    parser.add_argument('--limit', type=int, help='limit num of articles to add to DB')
    args = parser.parse_args()
    news_tab = news_tab_opts.get(args.news_tab)
    print(args, news_tab)


if __name__ == '__main__':
    main()