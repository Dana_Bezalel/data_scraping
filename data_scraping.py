import requests
from bs4 import BeautifulSoup, Comment

def get_html_from_link(link):
    # get the link
    html = requests.get(link)
    # make sure we got an OK response
    if html.status_code != requests.codes.ok:
        print(" response is not OK its:", html.status_code)
    if requests.codes['temporary_redirect'] != 307:
        print("temporary redirct is:", requests.codes['temporary_redirect'])
    if requests.codes.teapot != 418:
        print("requests.codes.teapot:", requests.codes.teapot)
    if requests.codes['o/']:
        print("requests.codes['o/']:", requests.codes['o/'])
    return html

def parse_content(html):
    # Get the Content
    text = html.text
    #print("text:", text)
    # headers:
    headers = html.headers
    #print("headers:", headers)
    # get url headers
    soup = BeautifulSoup(html.content, 'html.parser')
    #print(soup.prettify())
    try :
        title = str(soup.find_all(class_="art_header_title")).split('>')[1].split('<')[0]
        author = str(soup.find_all(class_="art_header_footer_author")).split('>')[2].split('<')[0]
        date_str = str(soup.find_all(class_="art_footer_date")).split("\"art_footer_date\">")[1].split(": ")[1].split('<')[0]
        print("Last update Date:", headers['Date'], "title:" , title, "autor:",  author, "Published Date:", date_str)
    except :
        print("Oops! couldnt extract data from this link...")

def main():
    link = "https://www.ynetnews.com/articles/0,7340,L-5624752,00.html"
    html = get_html_from_link(link)
    parse_content(html)

if __name__ == "__main__":
    main()