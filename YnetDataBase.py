"""
The program parse articles from https://www.ynetnews.com/ and returns the following per article:
article_title, publish data and time, modified data and time, title_Type (today_top_stories, news,
middle easts, ect.), publisher name
Authors: Dana_Bezalel, Mor Maayan
"""
import argparse
import get_data_from_links
import update_db
import requests


YNET_URL = "https://www.ynetnews.com/"
# URL_CLASS_MAIN_PAGE = "hdr_isr hdr_abr"
URL_CLASS_MAIN_PAGE = 'orangesLi'

def parse_args():
    """the function gets user's argument of whether to retrieve a specific New Tab or limit number of articles"""
    news_tab_opts = {'isn': 'Israel News', 'me': 'Middle East', 'w': 'World', 'm': 'Magazine',
                     'j': 'Jewish World',
                     'o': 'Opinion',
                     'c': 'Culture', 't': 'Travel', 'h': 'Health Science', 's': 'Science', 'b': 'Business',
                     't': 'Tech'}
    news_tab_help = 'chose news_tab to add to the database from :' + str(news_tab_opts)
    parser = argparse.ArgumentParser(description='get argument to limit search: --news_tab, --limit')
    parser.add_argument('--news_tab', choices=news_tab_opts.keys(), type=str, help=news_tab_help)
    parser.add_argument('--limit', type=int, help='limit num of articles to add to DB')
    parser.add_argument('--enrich', choices=['y', 'n'], type=str, help='whether to enrich the data from NYT API: "y" or "n"')
    args = parser.parse_args()
    news_tab = news_tab_opts.get(args.news_tab)

    return news_tab, args.limit, args.enrich


def main():
    # gets user's arguments (if exist)
    news_tab, limit, enrich = parse_args()

    try:
        # Get url of the main page of 'ynet.com' and returns a dictionary with the links to the News Tabs pages (e.g. Israeli News, Word, etc.)
        news_tab_links = get_data_from_links.get_news_tab_links(YNET_URL, URL_CLASS_MAIN_PAGE, news_tab)
        # Within each News Tabs retrieve the article links and date and print into scv file
        update_db.write_articles_data_to_db(news_tab_links, limit, enrich)

    except requests.exceptions.ConnectionError as err:
        print("Connection error: {}".format(err))

if __name__ == '__main__':
    main()
